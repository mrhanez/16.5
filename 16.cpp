﻿#include <iostream>
#include <vector>

int main() {
    int N = 5;
    std::vector<std::vector<int>> matrix(N, std::vector<int>(N));

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            matrix[i][j] = i + j;
        }
    }

    for (const auto& row : matrix) {
        for (const auto& elem : row) {
            std::cout << elem << " ";
        }
        std::cout << "\n";
    }

    int calendar_day = 12;
    int row = calendar_day % N;
    int sum = 0;
    for (auto elem : matrix[row]) {
        sum += elem;
    }
    std::cout << "The sum of the elements in the third row is " << sum << "\n";

    return 0;
}